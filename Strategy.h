#pragma once

#include "Point.h"
#include "Army.h"

class Strategy {
public:
    virtual ~Strategy() {};
    std::vector<Point> find_path(const Point& a, const Point& b);
    int choose_enemy();
    virtual Army* create_army(double resources) = 0;
};
