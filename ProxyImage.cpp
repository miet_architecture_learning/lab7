#include "ProxyImage.h"
#include "RealImage.h"
#include <eggx.h>
#include <memory>
#include <vector>

int ProxyImage::get_window() {
    this->create_real_image();
    return this->real_image->get_window();
}

void ProxyImage::set_window(int value) {
    this->create_real_image();
    this->real_image->set_window(value);
}

void ProxyImage::create_real_image() {
    if (!this->real_image) {
        this->real_image = std::make_unique<RealImage>();
    }
}

ProxyImage::~ProxyImage() {
    gclose(this->real_image->get_window());
}

void ProxyImage::draw_area(const Area& terr) {
    this->create_real_image();
    this->real_image->draw_area(terr);
}

void ProxyImage::draw(const std::vector<Area>&  regions, const std::vector<Nation*>& nations) {
    this->create_real_image();
    this->real_image->draw(regions, nations);
}

void ProxyImage::draw_army(const Army& army) {
    this->create_real_image();
    this->real_image->draw_army(army);
}
