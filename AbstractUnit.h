#pragma once
#include "Point.h"
class AbstractUnit {
    protected:
        Point coordinates;
    public:
        virtual ~AbstractUnit() {};
        virtual double get_value() const = 0;
        virtual void add_unit(AbstractUnit* abst_unit_ptr) = 0;
        virtual void remove_unit(AbstractUnit* abstr_unit_ptr) = 0;
        virtual void move() = 0;
        virtual void fight() = 0;
        virtual Point get_coordinates() const = 0;
        virtual void set_coordinates(const Point& a) {this->coordinates = a;};
};
