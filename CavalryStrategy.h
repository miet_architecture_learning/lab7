#include "Strategy.h"

class CavalryStrategy : public Strategy {
    Army* create_army(double resources) override;
};
