#pragma once

#include "Image.h"
#include "eggx.h"
#include <vector>
class RealImage : public Image {
    public:
        void draw(const std::vector<Area>& regions, const std::vector<Nation*>& nations) override;
        int get_window() override;
        void set_window(int value) override;
        void draw_area(const Area& terr) override;
        void draw_army(const Army& army) override;
};
