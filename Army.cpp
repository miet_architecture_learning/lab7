#include "Army.h"
#include <iostream>

void Army::set_path(const std::vector<Point>& value) {
    this->path = value;
}

void Army::move() {
    Point next = this->path[this->poin_on_path];
    for (unsigned long int i = 0; i != this->content.size(); i++) {
        this->content[i]->set_coordinates(next);
    }
    this->coordinates = next;
    this->poin_on_path++;
}

void Army::set_coordinates(const Point& a) {
    for (unsigned long int i = 0; i != this->content.size(); i++) {
        this->content[i]->set_coordinates(a);
    }
    this->coordinates = a;
}




