enum class TerrainType {
    Forest,
    Plain,
    Mountain
};
