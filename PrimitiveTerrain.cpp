#include "PrimitiveTerrain.h"

double PrimitiveTerrain::get_value() const {
    switch(this->type) {
        case TerrainType::Forest:
            return 2;
        case TerrainType::Plain:
            return 1;
        case TerrainType::Mountain:
            return 0.5;
        default:
            return 0;
    }
}

PrimitiveTerrain::PrimitiveTerrain(Point& a, TerrainType val) {
    this->coordinates = a;
    this->type = val;
}

