#include "PrimitiveSoldier.h"
#include <iostream>

double PrimitiveSoldier::get_value() const {
    switch(this->type) {
        case SoldierType::Infantry:
            return 1;
        case SoldierType::Archer:
            return 1.5;
        case SoldierType::Cavalry:
            return 2;
        default:
            return 0;
    }
}

void PrimitiveSoldier::move() {
}

void PrimitiveSoldier::fight() {

}

PrimitiveSoldier::PrimitiveSoldier(SoldierType value) {
    this->type = value;
}
