#include "ArcherStrategy.h"
#include "Army.h"
#include "PrimitiveSoldier.h"
#include <iostream>
Army* ArcherStrategy::create_army(double resources) {
    PrimitiveSoldier archer(SoldierType::Archer);
    int number = static_cast<int> (resources / archer.get_value());
    Army* archer_army_ptr = new Army;
    for (int i = 0; i != number; i++) {
        archer_army_ptr->add_unit(new PrimitiveSoldier(SoldierType::Archer));
    }
    return archer_army_ptr;
}

