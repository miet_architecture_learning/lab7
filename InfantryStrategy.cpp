#include "InfantryStrategy.h"
#include "Army.h"
#include "PrimitiveSoldier.h"
Army* InfantryStrategy::create_army(double resources) {
    PrimitiveSoldier infantry(SoldierType::Infantry);
    int number = static_cast<int> (resources / infantry.get_value());
    Army* infantry_army_ptr = new Army;
    for (int i = 0; i != number; i++) {
        infantry_army_ptr->add_unit(new PrimitiveSoldier(SoldierType::Infantry));
    }
    return infantry_army_ptr;
}

