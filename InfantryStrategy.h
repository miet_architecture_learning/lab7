#include "Strategy.h"

class InfantryStrategy : public Strategy {
    Army* create_army(double resources) override;
};
