#include "Nation.h"
#include <algorithm>
#include <iostream>
Area Nation::get_territory() {
    return this->territory;
}

Army Nation::get_power() {
    return this->power;
}

void Nation::make_move() {
    this->power.add_unit(this->strategy->create_army(this->territory.get_value()));
    if (this->enemies.size() != 0) {
        this->power.set_path(this->strategy->find_path(this->capital_coordinates, this->enemies[0]->get_capital_coordinates()));
    }
    this->power.move();
    //this->power.fight();
}

Point Nation::get_capital_coordinates() const {
    return this->capital_coordinates;
}

Nation::Nation(const std::string& name_of_nation, const Area& terr, Strategy* str, Point capital) {
    this->territory = terr;
    this->strategy = str;
    this->capital_coordinates = capital;
    this->name = name_of_nation;
    this->power.set_coordinates(capital);
}

void Nation::add_enemy(Nation* enemy) {
    this->enemies.push_back(enemy);
}

std::vector<Nation*> Nation::get_enemies() const {
    return this->enemies;
}

void Nation::remove_enemy(Nation* enemy) {
    auto to_remove = std::remove_if(this->enemies.begin(), this->enemies.end(), [enemy] (Nation* enemy_ptr) { return enemy_ptr == enemy;});
    this->enemies.erase(to_remove);
}

