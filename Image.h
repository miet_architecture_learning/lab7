#pragma once

#include <string>
#include <vector>
#include "Area.h"
#include "Army.h"
#include "Nation.h"
class Image {
    protected:
        int win;
        double width;
        double height;
    public:
        virtual void draw(const std::vector<Area>& regions, const std::vector<Nation*>& nations) = 0;
        virtual ~Image();
        virtual int get_window() = 0;
        virtual void set_window(int value) = 0;
        virtual void draw_area(const Area& terr) = 0;
        virtual void draw_army(const Army& army) = 0;
};
