#pragma once

#include "CompositeAbstractUnit.h"
#include "Point.h"

class Army : public CompositeAbstractUnit {
private:
    unsigned long int poin_on_path = 0;
    std::vector<Point> path;
public:
    Army() {};
    //void fight() override;
    void move() override;
    Point get_position() const;
    void set_coordinates(const Point& a) override;
    void set_path(const std::vector<Point>& value);
};

