#include "RealImage.h"
#include <eggx.h>
#include <iostream>
#include <vector>
#include "Area.h"
void RealImage::set_window(int value) {
    this->win = gopen(value, value);
    gsetbgcolor(this->win, "white");
    newpen(this->win, 10);
    this->width = 360;
    this->height = 360;
    layer(win, 0, 1);
}

int RealImage::get_window() {
    return this->win;
}

void RealImage::draw_area(const Area& terr) {
    Point temp;
    for (unsigned long int i = 0; i != terr.get_content().size(); i++) {
        temp = terr.get_content()[i]->get_coordinates();
        fillrect(this->win, temp.get_x() * 60, temp.get_y() * 60, 60., 60.);
    }
}

void RealImage::draw(const std::vector<Area>& regions, const std::vector<Nation*>& nations) {
    gclr(this->win);
    for (unsigned long int i = 0; i != regions.size(); i++){
        newpen(this->win, static_cast<int>(i) + 2);
        this->draw_area(regions[i]);
    }
    newpen(this->win, 0);
    for (unsigned long int i = 0; i != regions.size(); i++) {
        drawcirc(this->win,
                 regions[i].get_content()[3]->get_coordinates().get_x() * 60.0 + 30.,
                 regions[i].get_content()[3]->get_coordinates().get_y() * 60.0 + 30.,
                 30., 30.);
    }
    for (unsigned long int i = 0; i != nations.size(); i++) {
        this->draw_army(nations[i]->get_power()); 
    }
    copylayer(win, 1, 0);
    msleep(500);
}

void RealImage::draw_army(const Army& army) {
    std::vector<Point> positions;
    for (unsigned long int i = 0; i != army.get_content().size(); i++) {
        positions.push_back(army.get_content()[i]->get_coordinates());
    }
    for (unsigned long int i = 0; i != positions.size(); i++) {
        fillrect(this->win, positions[i].get_x() * 60.0 + 20.0, positions[i].get_y() * 60.0 + 10.0, 20.0, 40.0);
    }
}
