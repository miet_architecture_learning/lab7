#pragma once
#include "Image.h"
#include "RealImage.h"
#include <memory>

class ProxyImage : public Image {
    private:
        std::unique_ptr<RealImage> real_image = nullptr;
        void create_real_image();
    public:
        ~ProxyImage();
        int get_window() override;
        void set_window(int value) override;
        void draw_area(const Area& terr) override;
        void draw(const std::vector<Area>& regions, const std::vector<Nation*>& nations) override;
        void draw_army(const Army& army) override;

};
