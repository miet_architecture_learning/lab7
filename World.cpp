#include "World.h"
#include <algorithm>
#include "PrimitiveTerrain.h"
#include <iostream>
void World::add_nation(Nation* new_nation) {
    this->nations.push_back(new_nation);
}

void World::remove_nation(Nation* dead_nation) {
    auto dead = std::remove_if(this->nations.begin(),
                               this->nations.end(),
                               [dead_nation] (Nation* dead_ptr) {return dead_nation == dead_ptr;});
    this->nations.erase(dead);
}

void World::take_turn() {
    for (int i = 0; i != static_cast<int>(this->nations.size()); i++) {
        this->nations[static_cast<long unsigned int>(i)]->make_move();
    }
}

World::World() {
    this->width = 360;
    this->height = 360;
}

void World::create_regions() {
    Area one, two, three;
    std::vector<Point> one_coords = {Point(0.,0.),
        Point(1.,0.),
        Point(2.,0.),
        Point(3.,0.),
        Point(4.,0.),
        Point(5.,0.),
        Point(1.,1.),
        Point(2.,1.),
        Point(3.,1.),
        Point(4.,1.),
         Point(2.,2.),
         Point(3.,2.)
    };
    std::vector<Point> two_coords = {Point(0.,1.),
        Point(0.,2.),
        Point(0.,3.),
        Point(0.,4.),
        Point(0.,5.),
        Point(1.,2.),
        Point(1.,3.),
        Point(1.,4.),
        Point(1.,5.),
        Point(2.,3.),
         Point(2.,4.),
         Point(2.,5.)
    };
    std::vector<Point> three_coords = {Point(3.,3.),
        Point(3.,4.),
        Point(3.,5.),
        Point(4.,2.),
        Point(4.,3.),
        Point(4.,4.),
        Point(4.,5.),
        Point(5.,1.),
        Point(5.,2.),
        Point(5.,3.),
         Point(5.,4.),
         Point(5.,5.)
    };
    for (int i = 0; i != static_cast<int>(one_coords.size() * 2); i++) {
        one.add_unit(new PrimitiveTerrain(one_coords[static_cast<long unsigned int>(i)], TerrainType::Forest));
        two.add_unit(new PrimitiveTerrain(two_coords[static_cast<long unsigned int>(i)], TerrainType::Plain));
        three.add_unit(new PrimitiveTerrain(three_coords[static_cast<long unsigned int>(i)], TerrainType::Mountain));
    }
    this->regions.push_back(one);
    this->regions.push_back(two);
    this->regions.push_back(three);
}

std::vector<Area> World::get_regions() const {
    return this->regions;
}

bool World::end_of_game() {
    for (unsigned long int i = 0; i != this->nations.size(); i++) {
        std::cout << "Nation: " << this->nations[i]->get_name() << " has " << this->nations[i]->get_power().get_value() + this->nations[i]->get_territory().get_value() << " total power.\n";
    }
    return true;
}
