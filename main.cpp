#include "Nation.h"
#include "PrimitiveTerrain.h"
#include "ArcherStrategy.h"
#include "World.h"
#include "CavalryStrategy.h"
#include "InfantryStrategy.h"
#include <iostream>
int main() {
    World world;
    ArcherStrategy arch_str;
    InfantryStrategy inf_str;
    CavalryStrategy cav_str;
    world.create_regions();
    Nation forest("Forest", world.get_regions()[0], &arch_str, world.get_regions()[0].get_content()[3]->get_coordinates());
    Nation plain("Plain", world.get_regions()[1], &cav_str, world.get_regions()[1].get_content()[3]->get_coordinates());
    Nation mountain("Mountain", world.get_regions()[2], &inf_str, world.get_regions()[2].get_content()[3]->get_coordinates());

    forest.add_enemy(&plain);
    forest.add_enemy(&mountain);
    
    plain.add_enemy(&mountain);
    plain.add_enemy(&forest);

    mountain.add_enemy(&forest);
    mountain.add_enemy(&plain);

    world.add_nation(&forest);
    world.add_nation(&plain);
    world.add_nation(&mountain);

    for (int i =0; i != 3; i++) {
        world.take_turn();
        world.draw();
    }
    world.end_of_game();
    return 0;
}
