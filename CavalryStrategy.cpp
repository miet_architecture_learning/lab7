#include "CavalryStrategy.h"
#include "Army.h"
#include "PrimitiveSoldier.h"
Army* CavalryStrategy::create_army(double resources) {
    PrimitiveSoldier cavalry(SoldierType::Cavalry);
    int number = static_cast<int> (resources / cavalry.get_value());
    Army* cavalry_army_ptr = new Army;
    for (int i = 0; i != number; i++) {
        cavalry_army_ptr->add_unit(new PrimitiveSoldier(SoldierType::Cavalry));
    }
    return cavalry_army_ptr;
}

