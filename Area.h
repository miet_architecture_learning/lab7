#pragma once

#include "CompositeAbstractUnit.h"

class Area : public CompositeAbstractUnit {
public:
    std::vector<AbstractUnit*> get_content() const override {return this->content;};
};
