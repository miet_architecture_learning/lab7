cmake_minimum_required(VERSION 3.27.0)

project("lab7" VERSION 1.0 LANGUAGES CXX)

set (CMAKE_CXX_STANDARD 20)


add_compile_options(
    -Werror

    -Wall
    -Wextra
    -Wpedantic

    -Wcast-align
    -Wcast-qual
    -Wconversion
    -Wctor-dtor-privacy
    -Wenum-compare
    -Wfloat-equal
    -Wnon-virtual-dtor
    -Wold-style-cast
    -Woverloaded-virtual
    -Wredundant-decls
    -Wsign-conversion
    -Wsign-promo

    -O3
    
)

add_link_options(
    -leggx 
    -lX11 
    -lm
    )

set(SOURCE_FILES
    ArcherStrategy.cpp
Army.cpp
CavalryStrategy.cpp
CompositeAbstractUnit.cpp
Image.cpp
InfantryStrategy.cpp
main.cpp
Nation.cpp
PrimitiveAbstractUnit.cpp
PrimitiveSoldier.cpp
PrimitiveTerrain.cpp
ProxyImage.cpp
RealImage.cpp
Strategy.cpp
World.cpp
  )


if(NOT CMAKE_CXX_EXTENSIONS)
    set(CMAKE_CXX_EXTENSIONS OFF)
endif()

add_executable( lab7
  ${SOURCE_FILES}
  )

target_link_libraries(lab7 eggx)
