#pragma once

#include "AbstractUnit.h"
#include <vector>

class CompositeAbstractUnit : public AbstractUnit {
protected:
    std::vector<AbstractUnit*> content;
public:
    void add_unit(AbstractUnit* abs_unit_ptr) override;
    void remove_unit(AbstractUnit* abs_unit_ptr) override;
    double get_value() const override;
    void move() override {};
    void fight() override {};
    Point get_coordinates() const override {return this->coordinates;};
    virtual std::vector<AbstractUnit*> get_content() const {return this->content;};
};
