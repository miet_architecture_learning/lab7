#include "CompositeAbstractUnit.h"
#include "AbstractUnit.h"
#include <algorithm>
#include <cstdint>
double CompositeAbstractUnit::get_value() const {
    double res = 0.0;
    for (uint64_t i = 0; i != this->content.size(); i++) {
        res += this->content[i]->get_value();
    }
    return res;
}

void CompositeAbstractUnit::add_unit(AbstractUnit* abs_unit_ptr) {
    this->content.push_back(abs_unit_ptr);
}

void CompositeAbstractUnit::remove_unit(AbstractUnit* abs_unit_ptr) {
    auto to_remove = std::remove_if(this->content.begin(), this->content.end(), [abs_unit_ptr] (AbstractUnit* p) {return p == abs_unit_ptr;});
    this->content.erase(to_remove); 
}
