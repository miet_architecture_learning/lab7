#pragma once

class Point {
    private:
        double x;
        double y;
    public:
        Point() {};
        Point(double x, double y) {this->x = x; this->y = y;};
        double get_x() const {return this->x;};
        double get_y() const {return this->y;};
        void set_x(double value) {this->x = value;}; 
        void set_y(double value) {this->y = value;}; 
};
