#include "Strategy.h"

class ArcherStrategy : public Strategy {
    Army* create_army(double resources) override;
};
