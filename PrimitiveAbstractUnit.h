#pragma once

#include "AbstractUnit.h"
#include "Point.h"

class PrimitiveAbstractUnit : public AbstractUnit {
    protected:
        Point coordinates;
    public:
        Point get_coordinates() const override {return this->coordinates;};
        void move() override {};
        void fight() override {};
        void add_unit(AbstractUnit* p) override {delete p;};
        void remove_unit(AbstractUnit* p) override {delete p;};
};
