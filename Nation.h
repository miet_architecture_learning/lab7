#pragma once

#include "Area.h"
#include "Army.h"
#include "Strategy.h"
#include <string>

class Nation {
private:
    Area territory;
    Army power;
    Strategy* strategy = nullptr;
    std::vector<Nation*> enemies;
    Point capital_coordinates;
    std::string name;
public:
    Nation();
    Nation(const std::string& name_of_nation, const Area& terr, Strategy* str, Point capital);
    Area get_territory();
    Army get_power();
    void make_move();
    Point get_capital_coordinates() const;
    void add_enemy(Nation* enemy);
    std::vector<Nation*> get_enemies() const;
    void remove_enemy(Nation* enemy);
    std::string get_name() const {return this->name;};
};

