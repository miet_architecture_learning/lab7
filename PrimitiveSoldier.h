#pragma once

#include "SoldierType.h"
#include "PrimitiveAbstractUnit.h"

class PrimitiveSoldier : public PrimitiveAbstractUnit {
private:
    SoldierType type;
public:
    PrimitiveSoldier(SoldierType value);
    SoldierType get_type() const;
    void move() override;
    double get_value() const override;
    void fight() override;
};

