#pragma once

#include "PrimitiveAbstractUnit.h"
#include "TerrainType.h"
class PrimitiveTerrain : public PrimitiveAbstractUnit {
    private:
        TerrainType type;
    public:
        PrimitiveTerrain(Point& a, TerrainType val);
        double get_value() const override; 
};
