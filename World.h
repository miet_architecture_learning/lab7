#include "Nation.h"
#include <vector>
#include "ProxyImage.h"
class World {
    private:
        std::vector<Nation*> nations;
        std::vector<Area> regions;
        int width;
        int height;
        ProxyImage image;
public:
    void draw() {image.set_window(360); image.draw(this->regions, this->nations);};
    World();
    void add_nation(Nation* new_nation);
    void remove_nation(Nation* dead_nation);
    void take_turn();
    bool end_of_game();
    void create_regions();
    std::vector<Area> get_regions() const;
};

