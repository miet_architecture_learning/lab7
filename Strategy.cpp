#include "Strategy.h"
#include <cstdint>
#include <cmath>
#include <iostream>
std::vector<Point> Strategy::find_path(const Point& a, const Point& b) {
    std::vector<Point> path;
    Point temp;
    if (a.get_x() > b.get_x()) {
        for (uint64_t i = 0; i != static_cast<uint64_t>(a.get_x() - b.get_x()); i++) {
            temp.set_x(a.get_x() - 1);
            temp.set_y(a.get_y());
            path.push_back(temp);
        }
    }
    else {
        for (uint64_t i = 0; i != static_cast<uint64_t>(b.get_x() - a.get_x()); i++) {
            temp.set_x(a.get_x() + 1);
            temp.set_y(a.get_y());
            path.push_back(temp);
        }

    }

    if (a.get_y() > b.get_y()) {
        for (uint64_t i = 0; i != static_cast<uint64_t>(a.get_y() - b.get_y()); i++) {
            temp.set_y(a.get_y() - 1);
            temp.set_x(a.get_x());
            path.push_back(temp);
        }
    }
    else {
        for (uint64_t i = 0; i != static_cast<uint64_t>(b.get_y() - a.get_y()); i++) {
            temp.set_y(a.get_y() + 1);
            temp.set_x(a.get_x());
            path.push_back(temp);
        }

    }
    return path;
}

